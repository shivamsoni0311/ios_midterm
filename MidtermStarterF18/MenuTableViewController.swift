//
//  MenuTableViewController.swift
//  MidtermStarterF18
//
//  Created by parrot on 2018-11-14.
//  Copyright © 2018 room1. All rights reserved.
//

import UIKit

class MenuTableViewController: UITableViewController {

     var movies = ["Add Customer", "Make Deposite"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    //--------- Mandatory Table View Functions ------- //
    
    
    // tell ios how many rows you want in your table view
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return movies.count
    }
    
    // tell ios what it should display in each row of the tableview
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // change "myCell" to whatever you called your cell in the storyboard
        let cell = tableView.dequeueReusableCell(withIdentifier:"myCell", for:indexPath)
        
        // set the row's text to the current item in the movies array
        cell.textLabel?.text = movies[indexPath.row]
        
        return cell;
    }
    /*
    // enable deleting a row through swiping
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            print("Deleted")
            
            // LOGIC - remove the item from the Array
            self.movies.remove(at: indexPath.row)
            
            // UI - update the tableview
            self.tableView.deleteRows(at: [indexPath], with: .automatic)
            
            self.tableView.reloadData();
        }
    }*/
    
    
    // person is clicking on stuff
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // do stuff when person clicks on a row
        print("Person clicked on something")
        print(indexPath.row)
        print(movies[indexPath.row])
        
    }
    
    
    // MARK: navigation to a second page
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // If you want to segue to another screen,
        // sometimes you will need to know what row the
        // person clicked on
        
        // Use self.tableView.indexPathForSelectedRow to
        // get the clicked row
        var row = self.tableView.indexPathForSelectedRow
        print("Selected row: \(row)")
    }
    

}
